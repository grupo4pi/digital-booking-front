import Titles from "../Titles/Titles";
import './PoliticsProducts.css';

const PoliticsProducts = ({detail}) => {
  return (
    <>
        <Titles title="Qué tenés que saber"/>
        <div className="container-politics-products">
            <div>
                <h4>Normas de la casa</h4>
                <ul>
                    { detail.policies.housesRules.map(item =>(
                        <li key={item}>{item}</li>
                    ))}
                </ul>
            </div>
            <div>
                <h4>Salud y seguridad</h4>
                <ul>
                    { detail.policies.healthAndSecurity.map(item =>(
                        <li key={item}>{item}</li>
                    ))}
                </ul>
            </div>
            <div>
                <h4>Política de cancelación</h4>
                <ul>
                    { detail.policies.cancellationPoliticies.map(item =>(
                        <li key={item}>{item}</li>
                    ))}
                </ul>
            </div>
        </div>

    </>
  )
}

export default PoliticsProducts;
