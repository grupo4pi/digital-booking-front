import './Button.css';
import {Link} from "react-router-dom";

const Button = ({text, variant, pathTo}) => {
	const variantStyle = variant ? `btn-${variant}` : 'btn-light';
	return pathTo ?
		<Link className={`btn ${variantStyle}`} to={pathTo}>{text}</Link> :
		<button className={`btn ${variantStyle}`}>{text}</button>;
}

export default Button;
