import Header from "./Header/Header";
import Footer from "./Footer/Footer";
import Menu from "./Menu/Menu";
import ProductsList from "./ProductsList/ProductsList";
import CategoryList from "./CategoryList/CategoryList";
import SearchBar from "./SearchBar/SearchBar";
import Layout from "./Layout/Layout";

export {
	Header,
	Footer,
	Menu,
	ProductsList,
	CategoryList,
	SearchBar,
	Layout
};
