import categories from '../../utils/categories.json';
import Category from '../Category/Category';
import './CategoryList.css';

const CategoryList = () => {
	return (
		<div className='categories-section'>
			<h2>Buscar por tipo de alojamiento</h2>
			<div className='categories'>
				{categories.map((category) =>
					<Category key={category.id} category={category}/>
				)}
			</div>
		</div>
	)

}

export default CategoryList;
