import './FeaturesProducts.css';
import {
	FaTv,
    FaWifi,
    FaSwimmer,
    FaCrosshairs,
    FaCarAlt,
    FaSnowflake,
    FaPaw,
    FaShoppingBasket
} from "react-icons/fa";
import Titles from '../Titles/Titles';

const FeaturesProducts = ({detail}) => {
  return (
    <div className="container-features-product">
        <div className="container-features-product-description">
            <h3>Alojate en el corazón de {detail.location}</h3>
            <p>{detail.description}</p>
        </div>
        <Titles title="¿Qué ofrece este lugar?" />
        
        <div className="container-features-product-features">
            <div>
                <div className='icon'>
                    <FaShoppingBasket/> 
                </div>
                <p>{detail.features[0]}</p>
            </div>
            <div>
                <div className='icon'>
                    <FaCarAlt/> 
                </div>
                <p>{detail.features[1]}</p>
            </div>
            <div>
                <div className='icon'>
                    <FaSnowflake/> 
                </div>
                <p>{detail.features[2]}</p>
            </div>
            <div>
                <div className='icon'>
                    <FaWifi/> 
                </div>
                <p>{detail.features[3]}</p>
            </div>
            <div>
                <div className='icon'>
                    <FaTv/> 
                </div>
                <p>{detail.features[4]}</p>
            </div>
            <div>
                <div className='icon'>
                    <FaPaw/> 
                </div>
                <p>{detail.features[5]}</p>
            </div>
            <div>
                <div className='icon'>
                    <FaSwimmer/> 
                </div>
                <p>{detail.features[6]}</p>
            </div>
        </div>
        
    </div>
  )
}
export default FeaturesProducts;
