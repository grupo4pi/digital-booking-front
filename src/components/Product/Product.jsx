import './Product.css';
import {
	FaHeart,
	FaWifi,
	FaSwimmer,
	FaStar,
	FaMapMarkerAlt
} from "react-icons/fa";
import Button from "../Button/Button";

const Product = ({product}) => {
	const {id, image, category, description, location, title} = product;
    
	return (
		<div className="product">
			<div className="product-img">
				<img
					src={image}
					alt={category}
				/>
				<FaHeart/>
			</div>

			<div className="product-text">
				<div className="product-text-punctuation">
					<div>8</div>
					<span>Muy bueno</span>
				</div>
				<div className="product-text-category">
					<span>{category} </span>

					<FaStar/>
					<FaStar/>
					<FaStar/>
					<FaStar/>
					<FaStar/>
				</div>
				<h3>{title}</h3>
				<p className="product-text-location"><FaMapMarkerAlt/> {location} - <a>MOSTRAR EN EL MAPA</a></p>
				<div className="icons">
					<FaWifi/> <FaSwimmer/>
				</div>
				<p className="product-text-description">{description.substring(0, 99)} <a> más...</a></p>
				<Button text="Ver más" variant="dark"  pathTo={`/product/${id}`}/>
			</div>
		</div>
	)
}

export default Product;
