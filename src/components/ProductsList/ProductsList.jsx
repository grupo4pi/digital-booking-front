import products from '../../utils/products.json';
import Product from '../Product/Product';
import './ProductsList.css';

const ProductsList = () => {
	return (
		<div className='products-section'>
			<div className="products-container">
				<h2>Recomendaciones</h2>
				<div className='products'>
					{products.map((product) =>
						<Product key={product.id} product={product}/>
					)}
				</div>
			</div>
		</div>
	)
}

export default ProductsList;
