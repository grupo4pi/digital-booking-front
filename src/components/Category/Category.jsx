import './Category.css';

const Category = ({category}) => {
	return (
		<div className="category">
			<img
				src={category.image}
				alt={category.category}
			/>
			<div className="category__info">
				<h4>{category.category}</h4>
				<p>{category.amount} {category.category.toLowerCase()}</p>
			</div>
		</div>
	)
}

export default Category;
