import {useEffect, useRef, useState} from "react";
import {FaMapMarkerAlt} from "react-icons/fa";
import cities from '../../utils/ubicacion.json';
import './LocalitySearchBar.css';

const Dropdown = ({items, trigger}) =>
	<ul className="locality-search-bar__dropdown">
		{items
			.map((item, idx) =>
				<li key={item.id} onClick={() => trigger(item)}
					className={`${idx === items.length - 1 || 'primary-border-bottom'} locality-search-bar__item`}>
					<FaMapMarkerAlt/>
					<div>
						<p style={{color: "#000000"}}>{item.city}</p>
						<p>{item.country}</p>
					</div>
				</li>)}
	</ul>

const LocalitySearchBar = () => {
	const [isOpen, setIsOpen] = useState(false);
	const [selectedOption, setSelectedOption] = useState(null);
	const listRef = useRef();

	const checkIfClickedOutside = e => {
		if (isOpen && listRef.current && !listRef.current.contains(e.target)) {
			setIsOpen(false)
		}
	}

	let text = selectedOption ? `${selectedOption.city}, ${selectedOption.country}` : '¿A dónde vamos hoy?';

	useEffect(() => {
		document.addEventListener("mousedown", checkIfClickedOutside)
		return () => {
			document.removeEventListener("mousedown", checkIfClickedOutside)
		}
	}, [isOpen])

	const handleIsOpen = () => {
		setIsOpen(prev => !prev);
		if (!isOpen === true) {
			setSelectedOption(null)
		}
	}

	return (
		<div className="locality-search-bar" ref={listRef}>
			<div onClick={handleIsOpen} className={`locality-search-bar__text-box ${selectedOption && 'selected-val'}`}>
				<FaMapMarkerAlt className="locality-search-bar__icon"/>
				<span>{text}</span>
			</div>

			{isOpen &&
				<Dropdown
					items={cities}
					trigger={
						option => {
							setSelectedOption(option);
							handleIsOpen();
						}}/>}
		</div>);
}

export default LocalitySearchBar;


