import {useEffect, useState} from "react";

import {
	FaInstagram,
	FaTwitter,
	FaLinkedinIn,
	FaFacebook,
} from "react-icons/fa";
import Avatar from "../Avatar/Avatar";
import Button from "../Button/Button";
import {AiOutlineClose, AiOutlineMenu} from "react-icons/ai";
import {Link, useLocation} from "react-router-dom";
import "./Menu.css";
import navLinks from "../../utils/navLinks.json";

const Menu = ({loggedIn, user, closeTrigger}) => {
	const [isOpen, setIsOpen] = useState(false);
	const [currentLinks, setCurrentLinks] = useState([]);
	const location = useLocation();
	const {name, lastname} = user;

	const handleOpenMenu = () => {
		setIsOpen((prev) => !prev);
	};

	useEffect(() => {
		setCurrentLinks(navLinks.filter(link => link.route !== location.pathname));
	}, [location]);


	return (
		<>
			<div className="web-menu">
				{loggedIn ? (
					<Avatar
						name={name}
						lastname={lastname}
						closeTrigger={closeTrigger}
					/>
				) : (
					<div className="buttons">
						{
							currentLinks.map(link => <Button key={link.id} text={link.label} pathTo={link.route}/>)
						}
					</div>
				)}
			</div>

			<div className="mobile-menu">
				<AiOutlineMenu
					className="mobile-menu__icon"
					onClick={handleOpenMenu}
				/>
				<div className={`mobile-menu__container ${isOpen && "open"}`}>
					<div className="mobile-menu__header">
						<AiOutlineClose
							className="mobile-menu__icon"
							onClick={handleOpenMenu}
						/>
						{loggedIn ? (
							<Avatar name={name} lastname={lastname}/>
						) : (
							<p>MENÚ</p>
						)}
					</div>

					<div className="mobile-menu__body">
						{loggedIn ? (
							<div
								className="mobile-menu__body-logout"
								onClick={closeTrigger}
							>
								¿Deseas <span>cerrar sesión</span>?
							</div>
						) : (
							<>
								{currentLinks.map((link, idx) => (
									<div key={link.id}
										 className={
											 idx === currentLinks.length - 1
												 ? ""
												 : "secondary-border-bottom"
										 }>
										<Link onClick={handleOpenMenu} to={link.route}>{link.label}</Link>
									</div>
								))}
							</>
						)}
					</div>

					<div className="mobile-menu__footer">
						<FaFacebook/>
						<FaLinkedinIn/>
						<FaTwitter/>
						<FaInstagram/>
					</div>
				</div>
			</div>
		</>
	);
};

export default Menu;
