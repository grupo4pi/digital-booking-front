import {useState} from "react";
import {Header, Footer} from "../";
import Context from "../../context/context.js";

export default function Layout({children}) {
	const [state, setState] = useState({
		formRegistro: {
			name: "",
			lastName: "",
			email: "",
			password: "",
			confirmPassword: "",
		},
		formLogin: {
			email: "",
			password: "",
		},
		user: {
			isLog: false,
			name: "",
			lastName: "",
		}
	})

	const update = (key, val) => {
		setState({[key]: val});
	}

	return (
		<div className="layout">
			<Context.Provider value={{state: state, update: update}}>
				<Header/>
				<main>
					{children}
				</main>
				<Footer/>
			</Context.Provider>
		</div>
	)
}
