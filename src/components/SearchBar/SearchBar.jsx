import Button from "../Button/Button";
import DateSearchBar from "../DateSearchBar/DateSearchBar";
import LocalitySearchBar from "../LocalitySearchBar/LocalitySearchBar";
import "./SearchBar.css";

const SearchBar = () =>
	<div className="search-bar">
		<h1>Busca ofertas en hoteles, casas y mucho más </h1>
		<div className="filters">
			<LocalitySearchBar/>
			<DateSearchBar/>
			<Button text="Buscar" variant="dark"/>
		</div>
	</div>

export default SearchBar;
