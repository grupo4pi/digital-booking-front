import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css/bundle";
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/navigation";
import "swiper/css/thumbs";

import "./ThumbsSliderImages.css";

// import required modules
import { FreeMode, Navigation, Thumbs, Pagination } from "swiper";

const ThumbsSliderImages = ({detail}) => {
  const images = detail.images;
  const [thumbsSwiper, setThumbsSwiper] = useState(null);

  return (
    <div className="container-thumbs-slider-images-dektop">
      <Swiper
        style={{
          "--swiper-navigation-color": "#fff",
          "--swiper-pagination-color": "#fff",
        }}
        pagination={{
          type: "fraction",
        }}
        loop={true}
        spaceBetween={10}
        navigation={true}
        // thumbs={{ swiper: thumbsSwiper }}
        thumbs={{swiper: thumbsSwiper && !thumbsSwiper.destroyed ? thumbsSwiper : null}}
        modules={[FreeMode, Navigation, Thumbs, Pagination]}
        className="mySwiper2"
      >
        { images.map(item =>(
          <SwiperSlide key={item}>
            <img src={item} alt=""/>
          </SwiperSlide>
        ))}
      </Swiper>
      <Swiper
        onSwiper={setThumbsSwiper}
        loop={true}
        spaceBetween={10}
        slidesPerView={4}
        freeMode={true}
        watchSlidesProgress={true}
        modules={[FreeMode, Navigation, Thumbs]}
        className="mySwiper3"
      >
        { images.map(item =>(
          <SwiperSlide key={item}>
            <img src={item} alt=""/>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
}

export default ThumbsSliderImages;