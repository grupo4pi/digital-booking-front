import "./Input.css";

export default function Input({ label, value, type, name, handleChange}) {
  const customClass = 'container-input'

  return (
    <div className={`${customClass}`}>
      <label className={`${customClass}__label`} htmlFor={name}>{label}</label>
      <input onChange={handleChange} id={name} className={`${customClass}__input`} name={name} type={type} value={value} />
    </div>
  );
}
