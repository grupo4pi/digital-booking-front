import "./Avatar.css";
import {AiOutlineClose} from "react-icons/ai";

const Avatar = ({name, lastname, closeTrigger}) => {
	const initials = `${name.substring(0, 1)}${lastname.substring(0, 1)}`;

	return (
		<div className="account">
			<div className="account__logo">
				{initials}
			</div>
			<p className="account__text">
				<span>Hola,</span>
				<br/>
				{`${name} ${lastname}`}
			</p>
			<AiOutlineClose onClick={closeTrigger ?? null}/>
		</div>
	);
}

export default Avatar;
