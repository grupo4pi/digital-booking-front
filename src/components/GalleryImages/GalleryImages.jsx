import './GalleryImages.css';
import Button from "../Button/Button";
import Modal from "reactjs-popup";
import ThumbsSliderImages from "../ThumbsSliderImages/ThumbsSliderImages";

import React from 'react';
import Popup from 'reactjs-popup';
const GalleryImages = ({detail}) => {

   const images = detail.images
    
  return (
    <div className="container-images-product">
        <div>
           <img src={images[0]} alt=""/> 
        </div>
        <div>
           <img src={images[1]} alt=""/> 
        </div>
        <div>
           <img src={images[2]} alt=""/> 
        </div> 
        <div>
           <img src={images[3]} alt=""/> 
        </div>
        <div>
           <img src={images[4]} alt=""/> 
        </div>
        {/* <Modal className="container-modal" trigger={<button className="btn-light"> Ver más </button>}>
				<ThumbsSliderImages detail={detail} />
            <button
            className="button"
            onClick={() => {
              console.log('modal closed ');
              close();
            }}
          >
            close modal
          </button>

			</Modal> */}
         <Popup
            trigger={<button className="btn-light"> Ver más </button>}
            modal
            nested
            >
               {close => (
                  <div className="modal">                                      
                     <div className="content">
                     <button className="close" onClick={close}>&times;
                     </button> 
                       <ThumbsSliderImages detail={detail} />              
                     </div>
                  </div>
               )}
         </Popup>

    </div>
  )
}

export default GalleryImages;
