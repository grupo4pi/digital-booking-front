import "react-date-range/dist/styles.css";
import "./DateSearchBar.css";
import {DateRangePicker} from "react-date-range";
import {addDays, format} from "date-fns";
import {useEffect, useRef, useState} from "react";
import {FaRegCalendarMinus} from "react-icons/fa";

const DateSearchBar = () => {
	const [isOpen, setIsOpen] = useState(false);
	const [dates, setDates] = useState([
		{
			startDate: new Date(),
			endDate: addDays(new Date(), 7),
			key: 'selection'
		}
	]);
	const pickerRef = useRef();

	const checkIfClickedOutside = e => {
		if (isOpen && pickerRef.current && !pickerRef.current.contains(e.target)) {
			setIsOpen(false)
		}
	}

	useEffect(() => {
		document.addEventListener("mousedown", checkIfClickedOutside)
		return () => {
			document.removeEventListener("mousedown", checkIfClickedOutside)
		}
	}, [isOpen])

	return (
		<div className="date-search-bar" ref={pickerRef}>
			<div className="date-search-bar__text-box" onClick={() => setIsOpen(prev => !prev)}>
				<FaRegCalendarMinus/>
				<span>{`${format(dates[0].startDate, 'PP')} - ${format(dates[0].endDate, 'PP')}`}</span>
			</div>

			{isOpen && (
				<div className="date-search-bar__picker">
					<DateRangePicker
						onChange={item => setDates([item.selection])}
						showSelectionPreview={true}
						moveRangeOnFirstSelection={false}
						months={1}
						ranges={dates}
						direction="horizontal"
					/>
				</div>)
			}
		</div>
	)
}

export default DateSearchBar;

