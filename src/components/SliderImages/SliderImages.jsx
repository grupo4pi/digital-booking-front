import React, { useRef, useState } from "react";
import './SliderImages.css';
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

import "swiper/css/bundle";

// import required modules
import { Autoplay, Pagination } from "swiper";

const SliderImages = ({detail}) => {
    
  const images = detail.images;

  return (
    <div className="container-slider-images-mobile">
      <Swiper
        pagination={{
          type: "fraction",
        }}
        autoplay={{
            delay: 3000,
            disableOnInteraction: false,
          }}
        modules={[Autoplay, Pagination]}
        className="mySwiperMobile"
      >
        
            { images.map(item =>(
                <SwiperSlide key={item}>
                  <img src={item} alt=""/>
                </SwiperSlide>
            ))}
        
        
      </Swiper>
    </div>
  );
}


export default SliderImages;