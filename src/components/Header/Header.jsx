import {useState, useContext} from "react";
import logo from "../../assets/logo.svg";
import "./Header.css";
import users from "../../utils/usuarios.json";
import Menu from "../Menu/Menu";
import {Link} from "react-router-dom";
import Context from "../../context/context.js";


const Header = () => {
	const [loggedIn, setLoggedIn] = useState(true);
	const user = users[0];
	const context = useContext(Context);

	return (
		<header className="header">
			<Link to="/" className="header__slogan">
				<img src={logo} alt="Logo"/>
				<p>Sentite como en tu hogar</p>
			</Link>
			<Menu closeTrigger={() => setLoggedIn(false)} loggedIn={loggedIn} user={user}/>
		</header>
	);
};

export default Header;
