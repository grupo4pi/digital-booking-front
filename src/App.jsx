import {Route, Routes} from "react-router-dom";
import {Layout} from "./components";
import {Home, Login, Register, ProductDetail} from "./pages";
import "./App.css";

const App = () =>
	<Layout>
		<Routes>
			<Route path="/" element={<Home/>}/>
			<Route path="/login" element={<Login/>}/>
			<Route path="/register" element={<Register/>}/>
			<Route path="/product/:id" element={<ProductDetail/>}/>
		</Routes>
	</Layout>

export default App;
