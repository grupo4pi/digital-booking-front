import Home from "./Home/Home";
import Login from "./Login/Login";
import Register from "./Register/Register";
import ProductDetail from "./ProductDetail/ProductDetail.jsx";

export {
	Home,
	Login,
	Register,
	ProductDetail
};
