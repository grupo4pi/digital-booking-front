import FeaturesProducts from "../../components/FeaturesProducts/FeaturesProducts";
import GalleryImages from "../../components/GalleryImages/GalleryImages";
import PoliticsProducts from "../../components/PoliticsProducts/PoliticsProducts";
import SliderImages from "../../components/SliderImages/SliderImages";
import ThumbsSliderImages from "../../components/ThumbsSliderImages/ThumbsSliderImages";
import productsJson from '../../utils/products.json';
import { Navigate, useNavigate, useParams } from "react-router-dom";

import "./ProductDetail.css";
import products from "./../../data/products.json";
import { Link } from "react-router-dom";
import { MdOutlineArrowBackIos } from "react-icons/all.js";
import { FaMapMarkerAlt, FaStar } from "react-icons/fa";

const product = products[0];

const ProductDetail = () => {
	const { id } = useParams();
		
    const navigate = useNavigate();

	const productFromJson = productsJson.find( product => product.id == id );
    	
 	const onNavigateBack = () => {    
		navigate(-1);
	}
	
    if (!product) {
         return <Navigate to="/" />;
    } 

	const renderStarts = (quantity, color) => {
		return (
			[...Array(quantity)].map(() => (
				<FaStar color={color} />
			))
		)
	}
	return (
		<>
			<div className="product-header text-white bg-secondary-accent flex-row-ac-jc">
				<div>
					<p className="product-category text-accent">
						{product.category}
					</p>
					<h1 className="product-name">{product.name}</h1>
				</div>
				<Link to="/" className="text-white">
					<MdOutlineArrowBackIos />
				</Link>
			</div>

			<div className="product-location flex-row-ac-jc">
				<div className="product-city text-secondary fw-medium flex flex-jb">
					<FaMapMarkerAlt />
					<span>
						{product.city} <br />A 940m del Centro
					</span>
				</div>
				<div className="product-punctuation flex text-secondary">
					<div>
						<p className="fw-bold">Muy bueno</p>
						<p className="text-primary">
							{ renderStarts(product.stars, '#F0572D')  }
							{ renderStarts(5 - product.stars, '#BEBEBE')  }
						</p>
					</div>
					<div className="product-punctuation-text flex flex-ac">
						{product.punctuation}
					</div>
				</div>
			</div>

			<GalleryImages detail = { productFromJson }/>
			<SliderImages detail = { productFromJson }/>
			<FeaturesProducts detail = { productFromJson }/>
			<PoliticsProducts detail = { productFromJson }/>		 
		</>
	);
};

export default ProductDetail;
