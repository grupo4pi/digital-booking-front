import "./Register.css";
import Button from "../../components/Button/Button";
import Input from "../../components/Input/Input";
import {Link} from "react-router-dom";
import {useState, useEffect, useContext} from "react";
import Context from "../../context/context.js";

const Register = () => {
	const context = useContext(Context)
	const [form, setForm] = useState(context.state.formRegister);

	useEffect(() => {
		context.update("formRegister", form)
	}, [form]);

	const handleChange = (e) => {
		console.log(e);

		setForm({
			...form,
			[`${e.target.name}`]: e.target.value,
		});
	};

	return (
		<div className="outer-container">
			<form className="form-register">
				<h1 className="text-primary">Crear cuenta</h1>

				<div className="double-input">
					<Input
						type="text"
						label="Nombre"
						name="name"
						handleChange={handleChange}
					/>
					<Input
						type="text"
						label="Apellido"
						name="lastName"
						handleChange={handleChange}
					/>
				</div>

				<Input
					type="email"
					label="Correo Electronico"
					name="email"
					handleChange={handleChange}
				/>
				<Input
					type="password"
					label="Contraseña"
					name="password"
					handleChange={handleChange}
				/>
				<Input
					type="password"
					label="Confirmar Contraseña"
					name="confirmPassword"
					handleChange={handleChange}
				/>

				<div className="container-button">
					<Button variant="dark" text="Crear Cuenta"/>
					<p className="subtitle">
						¿Ya tienes una cuenta?{" "}
						<Link to="/login">Inicia sesión</Link>
					</p>
				</div>
			</form>
		</div>
	);
};

export default Register;
