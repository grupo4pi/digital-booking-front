import "./Home.css";
import {CategoryList, ProductsList, SearchBar} from "../../components";

const Home = () =>
	<>
		<SearchBar/>
		<div className="main-container">
			<CategoryList/>
			<ProductsList/>
		</div>
	</>

export default Home;
