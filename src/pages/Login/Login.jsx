import Input from "../../components/Input/Input";
import Button from "../../components/Button/Button";
import { Link } from "react-router-dom";
import { useState, useEffect, useContext } from "react";
import Context from "../../context/context.js";

const Login = () => {
	const context = useContext(Context);
	const [form, setForm] = useState(context.state.formLogin);

	useEffect(() => {
		context.update("formLogin", form);
	}, [form]);

	const handleChange = (e) => {
		console.log(e);

		setForm({
			...form,
			[`${e.target.name}`]: e.target.value,
		});
	};

	return (
		<div className="outer-container">
			<form className="form-register">
				<h1 className="text-primary"> Iniciar Sesión</h1>
				<Input type="email" label="Correo Electrónico" name="email" handleChange={handleChange } />
				<Input type="password" label="Contraseña" name="password" handleChange={handleChange } />
				<div className="container-button">
					<Button text="Ingresar" variant="dark" />
					<p className="subtitle">
						¿Aún no tenes cuenta?{" "}
						<Link to="/register">Registrate</Link>
					</p>
				</div>
			</form>
		</div>
	);
};
	
export default Login;
